# Object Store Driver
This is the object store driver package. It is used to cache schemas and query results. The `Interface` in `interface.go` is implemented by both the actual driver and the mock driver. This allows for the use of a mock object store in usecase testing in services.

## Creating a Driver
```go
import "git.science.uu.nl/datastrophe/objectstore"

objectStore := objectstore.NewDriver()

// Connect to the object store
// Pass in the minio address, username (accessKeyID) and password (accessKey)
objectStore.Connect("localhost:9000", "root", "DikkeDraak")
```

## Creating a Mock Driver
```go
import "git.science.uu.nl/datastrophe/objectstore"

mockObjectStore := objectstore.NewMockDriver()
```

## Putting Objects
```go
err := objectStore.Put(ctx context.Context, bucketName string, objectName string, objectSize int64, object io.Reader)
```

## Getting Objects
```go
objectReader, err := objectStore.Get(ctx context.Context, bucketName string, objectName string)
```

## Testing
To test the object store package we wrote a bash script. The bash script starts up a Minio Docker container on which the tests are performed. Once the tests are completed, the Docker container is killed and deleted. Code coverage can be found in the `cover.html` file.