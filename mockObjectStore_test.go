/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

// Tests for the mocked object store, these tests are almost exactly the same as for the real object store

package objectstore

import (
	"bytes"
	"context"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
Tests the mock set
	t: *testing.T, makes go recognize this as a test
*/
func TestMockSet(t *testing.T) {
	d := NewMockDriver().(*MockDriver)

	testTable := []struct {
		name       string
		bucketName string
		objectName string
		object     string
	}{
		{
			name:       "store string",
			bucketName: "bucket-1",
			objectName: "object-1",
			object:     "test string bucket 1 object 1",
		},
		{
			name:       "store string different object same bucket",
			bucketName: "bucket-1",
			objectName: "object-2",
			object:     "test string bucket 1 object 2",
		},
		{
			name:       "store string different bucket same object",
			bucketName: "bucket 2",
			objectName: "object-1",
			object:     "test string bucket 2 object 1",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			obj := []byte(tt.object)
			err := d.Put(context.Background(), tt.bucketName, tt.objectName, int64(len(obj)), bytes.NewReader(obj))

			assert.NoError(t, err)

			assert.Equal(t, *d.objects[tt.bucketName][tt.objectName], obj)
		})
	}
}

/*
Tests the mock get
	t: *testing.T, makes go recognize this as a test
*/
func TestValidMockGet(t *testing.T) {
	d := NewMockDriver().(*MockDriver)

	// Fill the object store with some objects
	objectTable := []struct {
		bucketName string
		objectName string
		object     string
	}{
		{
			bucketName: "bucket-1",
			objectName: "object-1",
			object:     "I am object 1",
		},
		{
			bucketName: "bucket-1",
			objectName: "object-2",
			object:     "I am object 2",
		},
		{
			bucketName: "bucket-2",
			objectName: "object-1",
			object:     "I am object 1 in bucket 2",
		},
	}

	for _, o := range objectTable {
		if bucket, ok := d.objects[o.bucketName]; ok {
			b := []byte(o.object)
			bucket[o.objectName] = &b
			continue
		}
		d.objects[o.bucketName] = make(map[string]*[]byte)

		b := []byte(o.object)
		d.objects[o.bucketName][o.objectName] = &b
	}

	testTable := []struct {
		name       string
		bucketName string
		objectName string
		object     []byte
		expectErr  bool
	}{
		{
			name:       "object 1 from bucket 1",
			bucketName: "bucket-1",
			objectName: "object-1",
			object:     []byte("I am object 1"),
		},
		{
			name:       "object 2 from bucket 1",
			bucketName: "bucket-1",
			objectName: "object-2",
			object:     []byte("I am object 2"),
		},
		{
			name:       "object 1 from bucket 2",
			bucketName: "bucket-2",
			objectName: "object-1",
			object:     []byte("I am object 1 in bucket 2"),
		},
		{
			name:       "invalid bucket",
			bucketName: "bucket-3",
			objectName: "object-1",
			expectErr:  true,
		},
		{
			name:       "invalid object",
			bucketName: "bucket-2",
			objectName: "object-2",
			expectErr:  true,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			reader, err := d.Get(context.Background(), tt.bucketName, tt.objectName)

			if tt.expectErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)

				b, _ := io.ReadAll(reader)

				assert.Equal(t, tt.object, b)
			}
		})
	}
}
