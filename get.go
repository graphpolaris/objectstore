/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package objectstore

import (
	"context"
	"errors"
	"io"

	"github.com/minio/minio-go/v7"
)

/*
Get gets the object from the object store
	ctx: context.Context, the context in which this operation takes place
	bucketName: string, name of the bucket this object is in
	objectName: string, name of the object
	Returns: io.Reader for the object and a possible error
*/
func (d *Driver) Get(ctx context.Context, bucketName string, objectName string) (io.Reader, error) {
	// Check if the schema bucket exists
	if bucketExists, err := d.client.BucketExists(ctx, bucketName); err != nil || !bucketExists {
		return nil, errors.New("Bucket does not exist")
	}

	// Get the object
	reader, err := d.client.GetObject(ctx, bucketName, objectName, minio.GetObjectOptions{})

	return reader, err
}
