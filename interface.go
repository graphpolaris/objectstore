/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package objectstore

import (
	"context"
	"io"
)

/*
The Interface models an object store with storage buckets
*/
type Interface interface {
	Connect(address, accessKeyID, accessKey string) error
	Get(ctx context.Context, bucketName string, objectName string) (io.Reader, error)
	Put(ctx context.Context, bucketName string, objectName string, objectSize int64, object io.Reader) error
}
