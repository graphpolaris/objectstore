/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package objectstore

import (
	"context"
	"io"

	"github.com/minio/minio-go/v7/pkg/lifecycle"

	"github.com/minio/minio-go/v7"
)

/*
Put puts an object into the object store
	ctx: context.Context, the context in which this operation takes place
	bucketName: string, the bucket to place the object into
	objectName: io.Reader, a reader for the object we want to place in the object store
	Returns a possible error
*/
func (d *Driver) Put(ctx context.Context, bucketName string, objectName string, objectSize int64, object io.Reader) error {
	if bucketExists, err := d.client.BucketExists(ctx, bucketName); err != nil {
		return err
	} else if !bucketExists {
		// Define bucket lifecycle config
		config := lifecycle.NewConfiguration()
		if bucketName == "cached-schemas" {
			config.Rules = []lifecycle.Rule{
				{
					ID:     bucketName,
					Status: "Enabled",
					Expiration: lifecycle.Expiration{
						Days: 31,
					},
				},
			}
		}
		if bucketName == "cached-queries" {
			config.Rules = []lifecycle.Rule{
				{
					ID:     bucketName,
					Status: "Enabled",
					Expiration: lifecycle.Expiration{
						Days: 1,
					},
				},
			}
		}

		// Create the bucket
		err = d.client.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{})
		d.client.SetBucketLifecycle(ctx, bucketName, config)

		if err != nil {
			return err
		}
	}

	// Put the object into the bucket
	_, err := d.client.PutObject(ctx, bucketName, objectName, object, objectSize, minio.PutObjectOptions{})

	return err
}
