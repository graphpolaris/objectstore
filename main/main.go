/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"time"

	"git.science.uu.nl/graphpolaris/objectstore"
)

/*
This is the main method, it executes the code for this service
*/
func main() {

	f, err := os.Create("mem.prof")
	if err != nil {
		log.Fatal("could not create memory profile: ", err)
	}
	defer f.Close() // error handling omitted for example

	s := objectstore.NewDriver()
	s.Connect("localhost:9000", "root", "DikkeDraak")

	t := struct {
		t1 string
		t2 string
	}{
		t1: "Hey",
		t2: "Hey2",
	}
	// Store the schema in the object storage
	b, err := json.Marshal(t)
	if err == nil {
		reader := bytes.NewReader(b)
		log.Println(int64(len(b)))

		// Create 30 second put context
		putSchemaContext, cancelPutSchema := context.WithTimeout(context.Background(), time.Second*30)
		err = s.Put(putSchemaContext, "cached-schemas", fmt.Sprintf("%s-%s", "testdb", "testclient"), int64(len(b)), reader)
		if err != nil {
			log.Println(err.Error())
		}
		cancelPutSchema()
	}

	pprof.WriteHeapProfile(f)
}
