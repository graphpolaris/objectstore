/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package objectstore

import (
	"bytes"
	"context"
	"errors"
	"io"
)

/*
MockDriver implements the ObjectStore interface (mock)
*/
type MockDriver struct {
	// The objects are stored in a map
	// First key is the bucket name, second key is the object name
	objects map[string]map[string]*[]byte

	throwGetError bool
	throwPutError bool
}

/*
NewMockDriver creates a new mock object store driver
	Return: the interface of the new mock driver
*/
func NewMockDriver() Interface {
	return &MockDriver{
		objects: make(map[string]map[string]*[]byte),
	}
}

/*
Connect opens a connection to the object store (mock)
	Return: an error if there is one
*/
func (d *MockDriver) Connect(address, accessKeyID, accessKey string) error {
	return nil
}

/*
Get gets an object from the object store (mock)
	ctx: context.Context, the context for the mock driver
	bucketName: string, the name of the bucket
	objectName: string, the name of the object
	Return: (io.Reader, error) returns the reader and an error if there is one
*/
func (d *MockDriver) Get(ctx context.Context, bucketName string, objectName string) (io.Reader, error) {
	if d.throwGetError {
		return nil, errors.New("Failed to get object from object store")
	}

	// Check if this bucket exists
	if bucket, ok := d.objects[bucketName]; ok {
		// Check if object exists
		if object, ok := bucket[objectName]; ok {
			// Create an io.Reader for the object in the object store
			return bytes.NewReader(*object), nil
		}

		// Object does not exist
		return nil, errors.New("Specified object does not exist")
	}

	// Bucket does not exist
	return nil, errors.New("Specified bucket does not exist")
}

/*
Put puts an object in the object store
	ctx: context.Context, the context for the mock driver
	bucketName: string, the name of the bucket
	objectName: string, the name of the object
	objectSize: int64, the size of the object
	object: io.Reader, the reader for the mock driver
	Return: error if there is one
*/
func (d *MockDriver) Put(ctx context.Context, bucketName string, objectName string, objectSize int64, object io.Reader) error {
	if d.throwPutError {
		return errors.New("Error putting object into object store")
	}

	// Read all of the object into memory
	b, err := io.ReadAll(object)
	if err != nil {
		return err
	}

	// Check if this bucket exists
	if bucket, ok := d.objects[bucketName]; ok {
		bucket[objectName] = &b
	} else {
		// Create the bucket
		d.objects[bucketName] = make(map[string]*[]byte)

		// Store the object
		d.objects[bucketName][objectName] = &b
	}

	return nil
}

/*
ToggleThrowGetError makes the Get method throw an error the next time it is called
*/
func (d *MockDriver) ToggleThrowGetError() {
	d.throwGetError = !d.throwGetError
}

/*
ToggleThrowPutError makes the Put method throw an error the next time it is called
*/
func (d *MockDriver) ToggleThrowPutError() {
	d.throwPutError = !d.throwPutError
}
