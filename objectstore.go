/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package objectstore

import (
	"context"
	"time"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

/*
Driver implements the ObjectStore interface
*/
type Driver struct {
	client *minio.Client
}

/*
NewDriver creates a new object store driver
	Return: the interface of the new driver
*/
func NewDriver() Interface {
	return &Driver{}
}

/*
Connect opens a connection to the object store
	Return: if there's an error
*/
func (d *Driver) Connect(address, accessKeyID, accessKey string) error {
	// Initialize Minio client object
	var err error
	d.client, err = minio.New(address, &minio.Options{
		Creds: credentials.NewStaticV4(accessKeyID, accessKey, ""),
	})
	if err != nil {
		return err
	}

	// Get a test bucket, and make sure we do not get a error
	testBucketContext, cancelTestBucket := context.WithTimeout(context.Background(), time.Second*5)
	defer cancelTestBucket()
	_, err = d.client.BucketExists(testBucketContext, "test")
	return err
}
