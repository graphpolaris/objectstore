#This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
#© Copyright Utrecht University (Department of Information and Computing Sciences)

# Start Minio docker container
DOCKER_ID=`docker run -d -p 9000:9000 \
  -e "MINIO_ROOT_USER=root" \
  -e "MINIO_ROOT_PASSWORD=DikkeDraak" \
  minio/minio server /data`

# Run the tests
go test -v -coverpkg=./... -coverprofile=cover.out ./...
go tool cover -html=cover.out -o cover.html

# Delete the cover.out file
rm cover.out

# Stop the container
docker kill $DOCKER_ID

# Remove the container
docker container rm $DOCKER_ID
